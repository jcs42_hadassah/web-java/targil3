/*
 * Copyright (c) 2020. Joey Mizrahi
 */

//----------------------------------------------------------------------------------------------------------------------
//wrapped functions
(function () {
    //------------------------------------------------------------------------------------------------------------------
    "use strict"; //instead of waiting for dom content to be loaded
    //------------------------------------------------------------------------------------------------------------------
    //global imports of elements and ids
    const submitbutton = document.getElementById("submitbutton");
//----------------------------------------------------------------------------------------------------------------------

    async function deleteHistory()
    {
        const content = {
            method: "delete",
            headers: {
                "Content-Type": "application/json"
            }
        };
        fetch('/api/history',content)
            .then(response => response.json())
            .then(data =>
            {
                try{
                    switch (data.status) {
                        case 200: //successful history delete
                            window.location.href = "/history";
                            break;
                        case 401: //session timeout
                            window.location.href = "/login";
                            break;
                        default: //unknown error
                            window.location.href = '/error';
                            break;
                    }
                }catch (e) { //unknown error
                    window.location.href='/error';
                }
            }).catch(() => //unknown error
            {
                window.location.href='/error';
            });
    }

//----------------------------------------------------------------------------------------------------------------------
    //adding even listeners.
    (function () {
        if(submitbutton === null) return;
        submitbutton.addEventListener("click", deleteHistory);
    })();
})();
//----------------------------------------------------------------------------------------------------------------------