package com.hadassah.demo.RestApi;

import com.hadassah.demo.ResponseObject;
import com.hadassah.demo.beans.AuthorizeSessionLogin;
import com.hadassah.demo.repos.GitHubUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * The only restApi as the targil requirements wanted us to do only a single fetch.
 * This Restful api is for deleting all the history of the {@link com.hadassah.demo.repos.GitHubUser} in the ex3 db
 */
@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
@RestController
public class DeleteHistory {
    /**
     * the {@link GitHubUserRepo} bean to access the mysql DB.
     */
    @Autowired
    private GitHubUserRepo gitHubUserRepo;

    /**
     * the {@link AuthorizeSessionLogin} session bean to check if hes still logged in
     */
    @Resource(name = "logincheck")
    private AuthorizeSessionLogin authorizeSessionLogin;

    /**
     * RestApi endpoint for deleting all the history of the {@link com.hadassah.demo.repos.GitHubUser} in the ex3 db
     * @return a {@link ResponseObject} to the client. when the session ended its false, error msg and status 404 for session timeout
     * When its still logged in then its: true, success msg and 200 status code
     */
    @ResponseBody
    @DeleteMapping("/api/history")
    ResponseObject purge(){
        if(!authorizeSessionLogin.isAuthorized())
            return new ResponseObject(false, "Session Login ended, need to login again", HttpStatus.UNAUTHORIZED.value());
        gitHubUserRepo.deleteAll();
        return new ResponseObject(true, "successfully deleted history", HttpStatus.OK.value());
    }
}



