/*
 * Copyright (c) 2020. Joey Mizrahi
 */

package com.hadassah.demo.controllers;

import com.hadassah.demo.beans.AuthorizeSessionLogin;
import com.hadassah.demo.beans.LoginForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.Resource;
import javax.validation.Valid;


/**
 * Controller that handles login and logout requests
 */
@SuppressWarnings({"SpringMVCViewInspection", "SameReturnValue"})
@Controller
public class LogInController {

    /**
     * the {@link AuthorizeSessionLogin} session bean to check if hes still logged in
     */
    @Resource(name = "logincheck")
    private AuthorizeSessionLogin authorizeSessionLogin;

    /**
     * get mapping for login which will return login view if not logged in, otherwise redirect to home page
     * @param model, to render the view
     * @return login view or redirect
     */
    @GetMapping("/login")
    public String getLogin( Model model) {
        if(authorizeSessionLogin.isAuthorized())
            return "redirect:/";
        model.addAttribute(new LoginForm());
        return "login";
    }

    /**
     * post mapping for login.
     * we get sent here with form.
     * validate the input, if error return login view with errors.
     * otherwise authenticate user and redirect him to main page
     * @param loginForm validated login form bean
     * @param result to validate {@link LoginForm}
     * @return login view if errors.
     * otherwise redirect to home
     */
    @PostMapping("/login")
    public String checkLogin(@Valid LoginForm loginForm, BindingResult result) {
        if(authorizeSessionLogin.isAuthorized())
            return "redirect:/";
        if(result.hasErrors()){
            return "login";
        }
        authorizeSessionLogin.setAuthorized(true);
        return "redirect:/";
    }

    /**
     * logout controller,
     * logs him out and redirects to login
     *
     * @return redirect to login get controller
     */
    @GetMapping("/logout")
    public String logout(){
        authorizeSessionLogin.setAuthorized(false);
        return "redirect:/login";
    }
}