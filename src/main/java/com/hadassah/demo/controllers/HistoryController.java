/*
 * Copyright (c) 2020. Joey Mizrahi
 */

package com.hadassah.demo.controllers;

import com.hadassah.demo.repos.GitHubUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Controller that handles history get requests
 */
@SuppressWarnings({"SpringJavaAutowiredFieldsWarningInspection", "SameReturnValue"})
@Controller
public class HistoryController {

    /**
     * the object that manages the DB of {@link com.hadassah.demo.repos.GitHubUser} objects
     */
    @Autowired
    private GitHubUserRepo gitHubUserRepo;

    /**
     * handling get request for history path.
     * returns the view that will display the 10 most popular
     * searched for github users in descending order by their search hit count
     * @param model to render model
     * @return history html
     */
    @GetMapping("/history")
    public String render_history(Model model){
        model.addAttribute("githubUsers", gitHubUserRepo.findFirst10ByOrderBySearchedDesc());
        return "history";
    }
}