/*
 * Copyright (c) 2020. Joey Mizrahi
 */

package com.hadassah.demo.controllers;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

/**
  this controller handles all exceptions/errors and displays a friendly
  error page/

  for example try the URL:
            http://localhost:8080/delete/456

  where 456 is an ID that does not exist in the database
 */
@Controller
public class MyErrorController implements ErrorController {

    /**
     * overrided error mapping to handle errors.
     * will return custom 404 page in case of 404 and custom 500 page other cases.
     * @param request client request
     * @return error page view
     */
    @RequestMapping("/error")
    public String handleError(HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        if (status != null) {
            Integer statusCode = Integer.valueOf(status.toString());
            if(statusCode == HttpStatus.NOT_FOUND.value())
                return "error-404";
        }
        return "error-500";
    }

    /**
     * returns the path to errors
     * @return path to error handling
     */
    @Override
    public String getErrorPath() {
        return "/error";
    }
}