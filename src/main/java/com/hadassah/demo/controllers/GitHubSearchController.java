/*
 * Copyright (c) 2020. Joey Mizrahi
 */

package com.hadassah.demo.controllers;

import com.hadassah.demo.repos.GitHubUser;
import com.hadassah.demo.repos.GitHubUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;

/**
 * Controller that handles search operations
 */
@SuppressWarnings({"SpringJavaAutowiredFieldsWarningInspection", "SameReturnValue"})
@Controller
public class GitHubSearchController {
    /**
     * constant to hold string format of the URI to github api
     */
    final String search_api = "https://api.github.com/users/%s";

    /**
     * the object that manages the DB of {@link com.hadassah.demo.repos.GitHubUser} objects
     */
    @Autowired
    private GitHubUserRepo gitHubUserRepo;

    /**
     * get request for "/" route.
     * @param model, to add the {@link GitHubUser} into the view
     * @return "search.html" view
     */
    @GetMapping("/")
    public String renderSearch( Model model) {
        model.addAttribute(new GitHubUser());
        return "search";
    }

    /**
     * post handler for main page.
     * come here when search is initiated. here we will add, update the DB
     * @param user gitub user entity created from form.
     * @param result to validate the {@link GitHubUser}
     * @param model to render the search page again
     * @param restTemplate to build the object from the api
     * @return search view
     */
    @PostMapping("/")
    public String search(@Valid GitHubUser user, BindingResult result, Model model, RestTemplate restTemplate) {

        if (result.hasErrors()) {
            return "search";
        }

        String username = user.getLogin();

        try {
            /**access api of given username and try to create {@link GitHubUser} */
            user = restTemplate.getForObject(String.format(this.search_api, username), GitHubUser.class);
        }
        /**come here when user doesnt exist. other exceptions will go to error controller*/
        catch (HttpClientErrorException e) {
            model.addAttribute("exists",false);
            return "search";
        }
        processUser(user,model);

        return "search";
    }

    /**
     * increment an existing user in the DB or add a new one to the DB
     * @param user the user received from form + api
     * @param model to render back the search view with the results
     */

    private void processUser(GitHubUser user, Model model){
        if (gitHubUserRepo.existsByLogin(user.getLogin()))
            this.gitHubUserRepo.findByLogin(user.getLogin()).increment(); //incrementing existing user
        else
            gitHubUserRepo.save(user); // creating a new user
        //adding user to view
        model.addAttribute(user);
        model.addAttribute("exists",true);
    }
}