package com.hadassah.demo.beans;


import org.springframework.stereotype.Component;
import java.io.Serializable;

/**
 * session level bean to check globally if logged in
 */
@Component
public class AuthorizeSessionLogin implements Serializable {

    /**
     * boolean for login, true if login, false if not
     */
    private boolean authorized = false;

    /**
     * empty constructor so can be resourced
     */
    public AuthorizeSessionLogin(){}


    /**
     * Gets authorized.
     *
     * @return Value of authorized.
     */
    public boolean isAuthorized() {
        return authorized;
    }

    /**
     * Sets new authorized.
     *
     * @param authorized New value of authorized.
     */
    public void setAuthorized(boolean authorized) {
        this.authorized = authorized;
    }
}