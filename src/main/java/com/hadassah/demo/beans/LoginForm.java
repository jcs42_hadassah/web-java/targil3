/*
 * Copyright (c) 2020. Joey Mizrahi
 */

package com.hadassah.demo.beans;

import com.hadassah.demo.validators.ValidLogin;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;



/**
 * A login form bean,
 * this is received from the form and is validated through constraints
 * including custom constraint {@link ValidLogin}
 */
@ValidLogin(message = "wrong username or password, try again.")
public class LoginForm {
    /**
     * username to login, auto validated
     */
    @NotNull(message = "Username is mandatory")
    @NotBlank(message = "Username is mandatory")
    private String username;
    /**
     * password for login
     */
    @NotNull(message = "password is mandatory")
    @NotBlank(message = "password is mandatory")
    private String password;

    /**
     * empty constructor so can be injected to controller
     */
    public LoginForm() { }

    /**
     * so can create through form
     * @param user username
     * @param pass password
     */
    public LoginForm(String user, String pass) {
        this.setUsername(user);
        this.setPassword(pass);
    }


    /**
     * Sets new username to login, auto validated.
     *
     * @param username New value of username to login, auto validated.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Sets new password for login.
     *
     * @param password New value of password for login.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets username to login, auto validated.
     *
     * @return Value of username to login, auto validated.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Gets password for login.
     *
     * @return Value of password for login.
     */
    public String getPassword() {
        return password;
    }
}
