package com.hadassah.demo.repos;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.net.URI;

/**
 * The Github user entity that goes into the db.
 * This object is created through form of the search.html view.
 * The {@link GitHubUserRepo} handles instanced of this object in the DB.
 * We automatically create this object also through {@link org.springframework.web.client.RestTemplate#getForObject(URI, Class)} function
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class GitHubUser {
    /**
     * a unique id generated for each instance to be saved in DB
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    /**
     * the login for the github user, it is validated to be required and not empty
     */
    @NotNull(message = "github username is mandatory")
    @NotBlank(message = "github username cannot be blank!")
    private String login;
    /**
     * url to this user's github page
     */
    private String html_url;

    /**
     * the ammount of followers for this user.
     * Marked as transient as to not save into the DB
     */
    @Transient
    private int followers;
    /**
     * a hitcount of how many times this github user was searched for through our site
     */
    private int searched =1;

    /**
     * an empty constructor so it can get created for the form
     */
    public GitHubUser(){

    }

    /**
     * a constructor to create this with username from the form submit
     * @param username string representing the username login
     */
    public GitHubUser(String username){
        this.setLogin(username);
    }


    /**
     * Gets followers.
     *
     * @return Value of followers.
     */
    public int getFollowers() {
        return followers;
    }

    /**
     * Sets new followers.
     *
     * @param followers New value of followers.
     */
    public void setFollowers(int followers) {
        this.followers = followers;
    }

    /**
     * Gets username.
     *
     * @return Value of username.
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets new username.
     *
     * @param username New value of username.
     */
    public void setLogin(String username) {

        this.login = username.trim().toLowerCase();
    }

    public void increment(){
        this.searched++;
    }

    /**
     * Gets link.
     *
     * @return Value of link.
     */
    public String getHtml_url() {
        return html_url;
    }

    /**
     * Sets new link.
     *
     * @param link New value of link.
     */
    public void setHtml_url(String link) {
        this.html_url = link;
    }


    /**
     * Gets searched.
     *
     * @return Value of searched.
     */
    public int getSearched() {
        return searched;
    }

    /**
     * Sets new searched.
     *
     * @param searched New value of searched.
     */
    public void setSearched(int searched) {
        this.searched = searched;
    }
}
