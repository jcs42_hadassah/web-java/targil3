/*
 * Copyright (c) 2020. Joey Mizrahi
 */

package com.hadassah.demo.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * will give us access to the ex3 DB and allow us to handle {@link GitHubUser} objects within it.
 */
public interface GitHubUserRepo extends JpaRepository<GitHubUser, Long> {
    /**
     * will find a {@link GitHubUser} by its {@link GitHubUser login value}
     * @param login a username to search for
     * @return the {{@link GitHubUser} object or null
     */
    GitHubUser findByLogin(String login);

    /**
     * gets the 10 most popular searched for github users in descending order by their search hit count
     * @return list of {@link GitHubUser} sorted as explained above
     */
    List <GitHubUser> findFirst10ByOrderBySearchedDesc();

    /**
     * given a username, checks if that user exists in the DB (meaning hes been searched for before)
     * @param login a string that is the username of the guithub user
     * @return true if exists in db, false if not
     */
    boolean existsByLogin(String login);
}
