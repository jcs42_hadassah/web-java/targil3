/*
 * Copyright (c) 2020. Joey Mizrahi
 */

package com.hadassah.demo;

import com.hadassah.demo.beans.AuthorizeSessionLogin;
import com.hadassah.demo.repos.GitHubUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;

/**
 * the main class for the application
 */
@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
@SpringBootApplication
public class DemoApplication {
    /**
     * main function for the app
     * @param args, cmd line args to the program
     */
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    /**the repo for the table of github user in db ex3. this is global and by default this is singleton*/
    @Autowired
    public GitHubUserRepo gitHubUserRepo;

    /**Session level bean resource that will allow us to update/check login status*/
    @Resource(name = "logincheck")
    private AuthorizeSessionLogin authorizeSessionLogin;

    /**
     * defining the {@link AuthorizeSessionLogin} resource and its "factory" function
     * @return a new instance of {@link AuthorizeSessionLogin}
     */
    @Bean
    @Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public AuthorizeSessionLogin logincheck(){
        return new AuthorizeSessionLogin();
    }


}
