package com.hadassah.demo;

/**
 * A simple object that will be used to return from RestApi's
 * This is a good practice for api interface to return a status code as well as message
 */
public class ResponseObject {
    /**so the client can check if his session is still valid*/
    private boolean session_valid;
    /**the message to be returned to client*/
    private String message;
    /**the status code to be returned to client*/
    private int status;

    /**
     * Constructor.
     * @param logged_in, if session still valid
     * @param msg the message to be returned to user
     * @param status the status code to be returned to client
     */
    public ResponseObject(boolean logged_in, String msg,int status ){
        this.setMessage(msg);
        this.setSession_valid(logged_in);
        this.setStatus(status);
    }

    /**
     * Gets status.
     *
     * @return Value of status.
     */
    public int getStatus() {
        return status;
    }

    /**
     * Sets new message.
     *
     * @param message New value of message.
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Sets new session_valid.
     *
     * @param session_valid New value of session_valid.
     */
    public void setSession_valid(boolean session_valid) {
        this.session_valid = session_valid;
    }

    /**
     * Gets session_valid.
     *
     * @return Value of session_valid.
     */
    public boolean isSession_valid() {
        return session_valid;
    }

    /**
     * Gets message.
     *
     * @return Value of message.
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets new status.
     *
     * @param status New value of status.
     */
    public void setStatus(int status) {
        this.status = status;
    }
}
