/*
 * Copyright (c) 2020. Joey Mizrahi
 */

package com.hadassah.demo.filters;


import com.hadassah.demo.beans.AuthorizeSessionLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * this class intercepts all requests and displays statistics: request processing duration
 * look also at the MyConfig class that registers the filter and define the specific URL pattern to
 * catch.
 */

public class LoginInterceptor implements HandlerInterceptor {

    /**
     * an {@link AuthorizeSessionLogin} reference to the session level bean so i can
     * validate the login through this interceptoy
     */
    private AuthorizeSessionLogin authorizeSessionLogin = null;

    /**
     * to construct the interceptor
     * @param authorizeSessionLogin, session level bean received from config to validate login
     */
    public LoginInterceptor(AuthorizeSessionLogin authorizeSessionLogin) {
        this.authorizeSessionLogin = authorizeSessionLogin;
    }

    /**
     * requests that were not excluded in the {@link com.hadassah.demo.MyConfig#addInterceptors(InterceptorRegistry)} function
     * will come here and get redirected to login if they are not logged in
     * @param request client request
     * @param response client response
     * @param handler the object handler
     * @return true if valid login, false if not
     * @throws Exception in case redirect failure
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {

        if(!this.authorizeSessionLogin.isAuthorized() ){
            response.setStatus(440);
            response.sendRedirect("/login");
            return false;
        } else
            return true;
    }
}