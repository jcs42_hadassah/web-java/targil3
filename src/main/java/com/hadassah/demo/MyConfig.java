/*
 * Copyright (c) 2020. Joey Mizrahi
 */

package com.hadassah.demo;

import com.hadassah.demo.beans.AuthorizeSessionLogin;
import com.hadassah.demo.filters.LoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;
import java.util.Arrays;


/**
  this is a class for configuring SringMVC
  here we register our interceptor class and the session listener
  WebMvcConfigurer allows configuring all of the MVC:
 */

@Configuration
public class MyConfig implements WebMvcConfigurer {

    /**this session scoped bean is to check if login is still in session*/
    @Resource(name = "logincheck")
    private AuthorizeSessionLogin authorizeSessionLogin;

    /**
     * Adds the interceptors of the program
     * Will add the login interceptor, excluding certain paths.
     * @param registry {@link InterceptorRegistry} login interceptor
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry){
        registry.addInterceptor(new LoginInterceptor(authorizeSessionLogin)).addPathPatterns("/*").excludePathPatterns(Arrays.asList("/login", "/error", "/logout"));
    }
}