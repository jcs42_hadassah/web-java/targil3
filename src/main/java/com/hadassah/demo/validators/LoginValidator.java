/*
 * Copyright (c) 2020. Joey Mizrahi
 */

package com.hadassah.demo.validators;

import com.hadassah.demo.beans.LoginForm;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * implements {@link ConstraintValidator} and validates {@link LoginForm} using the {@link ValidLogin} constraint
 */
public class LoginValidator implements ConstraintValidator<ValidLogin, LoginForm> {
    /**using the value defined in application properties as the only correct username*/
    @Value("${spring.security.user.name}")
    private String test_username;
    /**using the value defined in application properties as the only correct password*/
    @Value("${spring.security.user.password}")
    private String test_password;

    /**
     * Initialize fun for this validator
     * @param constraintAnnotation, {@link ValidLogin} constraint
     */
    public void initialize(ValidLogin constraintAnnotation) {

    }

    /**
     * will check if given {@link LoginForm} has valid username and password
     * @param login, the {@link LoginForm} Bean received from client side
     * @param constraintValidatorContext default param for override
     * @return true if the username and password are valid. False otherwise
     */
    @Override
    public boolean isValid(LoginForm login,
                           ConstraintValidatorContext constraintValidatorContext) {

        String pass  = login.getPassword();
        String username = login.getUsername();
        return pass.equals(test_password) && username.equals(test_username);
    }
}
