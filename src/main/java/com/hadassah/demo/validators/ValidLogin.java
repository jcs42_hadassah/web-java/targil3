/*
 * Copyright (c) 2020. Joey Mizrahi
 */

package com.hadassah.demo.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * An interface for checking login validity.
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = { LoginValidator.class })
public @interface ValidLogin {
    String message() default "{com.example.validators.ValidLogin.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}